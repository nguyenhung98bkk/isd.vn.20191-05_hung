*Huyền reviews flow of events for Hùng*
1. Về trình bày
- Trình bày đẹp, rõ ràng, tương đối dễ hiểu
2. Về nội dung
-Usecase "Exit the platform area"
    + Tiền điều kiện " đã đi tàu đến bến cần xuống" là không cần thiết.
    + Thứ tự thực hiện action 3, 4 bị ngược nhau. Theo mình thứ tự đúng
    cần là 3. Gate: thực hiện mở cổng
           4. System: Hiển thị " Openning ticket/ card"
           5. System: Hiển thị thông tin cơ bản của vé/ thẻ.
-Usecase " Scan prepaid card"
    + Luồng thay thế 2a,2b của hành động " kiểm tra số tiền trong thẻ"
    chỉ đúng khi khách rời khỏi sân ga, còn khi khách vào sân ga cần 
    kiểm tra số dư trong thẻ có lớn hơn hoặc bằng giá vé cơ bản hay không
    + Vì vậy theo mình trước tiên hệ thống cần xác định được là khách vào 
    ga hay rời ga đã.

